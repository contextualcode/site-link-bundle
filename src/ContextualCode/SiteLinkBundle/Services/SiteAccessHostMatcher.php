<?php

namespace ContextualCode\SiteLinkBundle\Services;

use eZ\Publish\API\Repository\Values\Content\Location;
use eZ\Publish\Core\MVC\ConfigResolverInterface;
use eZ\Publish\Core\MVC\Symfony\SiteAccess;
use eZ\Publish\Core\MVC\Symfony\SiteAccess\SiteAccessProviderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

class SiteAccessHostMatcher
{
    /** @var Request */
    protected $request;
    /** @var array */
    protected $eZSiteaccessRelationMap;
    /** @var array */
    protected $eZSiteaccessMatchConfig;
    /** @var SiteAccessProviderInterface */
    private $provider;
    /** @var ConfigResolverInterface */
    private $configResolver;
    /** %ezpublish.siteaccess.default% String */
    private $defaultSiteaccess;

    public function __construct(
        RequestStack $requestStack,
        array $eZSiteaccessMatchConfig,
        SiteAccessProviderInterface $provider,
        ConfigResolverInterface $configResolver,
        String $defaultSiteaccess
    ) {
        $this->request = $requestStack->getMasterRequest();
        $this->eZSiteaccessMatchConfig = $eZSiteaccessMatchConfig;
        $this->provider = $provider;
        $this->configResolver = $configResolver;
        $this->defaultSiteaccess = $defaultSiteaccess;
    }

    protected function setup()
    {
        if (isset($this->eZSiteaccessRelationMap)) {
            return;
        }
        $this->eZSiteaccessRelationMap = $this->getAllSiteAccessesRelation();
        foreach ($this->eZSiteaccessRelationMap as $key => $val) {
            if (empty($key)) {
                $this->eZSiteaccessRelationMap['default'] = $val;
                unset($this->eZSiteaccessRelationMap[null]);
            }
        }
        $keys = array_keys($this->eZSiteaccessRelationMap);
        $this->repoName = count($keys) ? $keys[0] : null;
    }

    // copied from ezsystems/ezplatform-kernel/eZ/Publish/Core/MVC/Symfony/SiteAccess/SiteAccessService.php
    // getSiteAccessesRelation(), but modified to return all
    public function getAllSiteAccessesRelation()
    {
        $saRelationMap = [];

        /** @var SiteAccess[] $saList */
        $saList = iterator_to_array($this->provider->getSiteAccesses());
        // First build the SiteAccess relation map, indexed by repository and rootLocationId.
        foreach ($saList as $sa) {
            $siteAccessName = $sa->name;

            $repository = $this->configResolver->getParameter('repository', 'ezsettings', $siteAccessName);
            if (!isset($saRelationMap[$repository])) {
                $saRelationMap[$repository] = [];
            }

            $rootLocationId = $this->configResolver->getParameter('content.tree_root.location_id', 'ezsettings', $siteAccessName);
            if (!isset($saRelationMap[$repository][$rootLocationId])) {
                $saRelationMap[$repository][$rootLocationId] = [];
            }

            $saRelationMap[$repository][$rootLocationId][] = $siteAccessName;
        }

        return $saRelationMap;
    }

    public function getRequest()
    {
        return $this->request;
    }

    /**
     * Determine best site access for given location.
     *
     * @return string|null
     */
    public function getLocationSiteAccess(Location $location)
    {
        $this->setup();
        if ($this->repoName) {
            $siteaccessRoots = array_keys($this->eZSiteaccessRelationMap[$this->repoName]);
            foreach (array_reverse($location->path) as $locationId) {
                if (in_array($locationId, $siteaccessRoots)) {
                    foreach ($this->eZSiteaccessRelationMap[$this->repoName][$locationId] as $match) {
                        if ($match == $this->defaultSiteaccess) return $match;
                    }
                    return $this->eZSiteaccessRelationMap[$this->repoName][$locationId][0];
                }
            }
        }

        return null;
    }

    /**
     * Determine root location id for given location's site access.
     *
     * @return int|null
     */
    public function getLocationRootLocationId(Location $location)
    {
        $this->setup();
        // determine what site access Location belongs to
        if ($this->repoName) {
            $siteaccessRoots = array_keys($this->eZSiteaccessRelationMap[$this->repoName]);
            foreach (array_reverse($location->path) as $locationId) {
                if (in_array($locationId, $siteaccessRoots)) {
                    return $locationId;
                }
            }
        }

        return null;
    }

    /**
     * Attempt to find a siteaccess host that is closest to the
     * current host.
     *
     * @param string $siteaccess
     *
     * @return string
     */
    public function matchBestHost($siteaccess)
    {
        $this->setup();
        if (!$this->request) {
            return '';
        }
        $currentHost = $this->request->getHost();
        if (!isset($this->eZSiteaccessMatchConfig['Map\Host'])) {
            return '';
        }
        $currentHostParts = explode('.', trim($currentHost));
        $bestMatch = '';
        $bestMatchPartCount = 0;
        foreach ($this->eZSiteaccessMatchConfig['Map\Host'] as $matchHost => $matchSiteaccess) {
            if ($matchSiteaccess != $siteaccess) {
                continue;
            }
            $hostParts = explode('.', trim($matchHost));
            $partCount = count(array_intersect($currentHostParts, $hostParts));
            if ($partCount > $bestMatchPartCount || !$bestMatch) {
                $bestMatchPartCount = $partCount;
                $bestMatch = $matchHost;
            }
        }

        $r = sprintf(
            '%s://%s%s',
            $this->request->getScheme(),
            $bestMatch ?: $currentHost,
            (!in_array($this->request->getPort(), [80, 443]) ? ':' . $this->request->getPort() : '')
        );

        return rtrim($r, '/');
    }

    /**
     * Get first siteaccess host.
     *
     * @param string $siteaccess
     *
     * @return string
     */
    public function matchFirstHost($siteaccess)
    {
        $this->setup();
        if (!$this->request) {
            return '';
        }
        $currentHost = $this->request->getHost();

        $bestMatch = '';
        $uri = '';
        if (isset($this->eZSiteaccessMatchConfig['Map\Host'])) {
            foreach ($this->eZSiteaccessMatchConfig['Map\Host'] as $matchHost => $matchSiteaccess) {
                if ($matchSiteaccess != $siteaccess) {
                    continue;
                }
                $bestMatch = $matchHost;
                break;
            }
        }

        if (
            empty($bestMatch) &&
            isset($this->eZSiteaccessMatchConfig['Map\URI'])
        ) {
            foreach ($this->eZSiteaccessMatchConfig['Map\URI'] as $mapURI => $matchSiteaccess) {
                if ($matchSiteaccess != $siteaccess) {
                    continue;
                }
                $uri = $mapURI;
                $bestMatch = $currentHost;
                break;
            }
        }

        if (
            empty($bestMatch) &&
            isset($this->eZSiteaccessMatchConfig['Compound\LogicalAnd'])
        ) {
            foreach ($this->eZSiteaccessMatchConfig['Compound\LogicalAnd'] as $name => $config) {
                $matchSiteaccess = $config['match'] ?? '';
                if (empty($matchSiteaccess) || $matchSiteaccess != $siteaccess) {
                    continue;
                }

                $matchers = $config['matchers'] ?? [];
                if (count($matchers) === 0) {
                    continue;
                }

                if (isset($matchers['Map\URI'])) {
                    $uriKeys = array_keys($matchers['Map\URI']);
                    $uri = $uriKeys[0];
                }
                if (isset($matchers['Map\Host'])) {
                    $hostKeys = array_keys($matchers['Map\Host']);
                    $bestMatch = $hostKeys[0];
                }

                break;
            }
        }

        $r = sprintf(
            '%s://%s%s/%s',
            $this->request->getScheme(),
            $bestMatch ?: $currentHost,
            (!in_array($this->request->getPort(), [80, 443]) ? ':' . $this->request->getPort() : ''),
            $uri
        );

        return rtrim($r, '/');
    }
}
