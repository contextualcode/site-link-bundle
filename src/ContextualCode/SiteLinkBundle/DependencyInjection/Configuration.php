<?php

namespace ContextualCode\SiteLinkBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder('contextual_code_site_link');

        $rootNode = $treeBuilder->getRootNode();
        $rootNode
            ->children()
                ->enumNode('siteaccess_host_match_method')
                    ->values(['none', 'first', 'best'])
                    ->defaultValue('first')
                ->end()
                ->arrayNode('internal_external')
                    ->prototype('array')
                        ->children()
                            ->scalarNode('class_identifier')
                                ->isRequired()
                                ->cannotBeEmpty()
                            ->end()
                            ->scalarNode('internal_attribute_identifier')
                                ->cannotBeEmpty()
                            ->end()
                            ->scalarNode('external_attribute_identifier')
                                ->cannotBeEmpty()
                            ->end()
                        ->end()
                    ->end()
                ->end()
                ->arrayNode('ancestor_link')
                    ->prototype('array')
                        ->children()
                            ->scalarNode('class_identifier')
                            ->end()
                            ->scalarNode('ancestor_level')
                            ->end()
                        ->end()
                    ->end()
                ->end()
                ->arrayNode('direct_file_link')
                    ->prototype('array')
                        ->children()
                            ->scalarNode('class_identifier')
                            ->end()
                            ->scalarNode('attribute_identifier')
                            ->end()
                        ->end()
                    ->end()
                ->end()
                ->arrayNode('direct_image_link')
                    ->prototype('array')
                        ->children()
                            ->scalarNode('class_identifier')
                            ->end()
                            ->scalarNode('attribute_identifier')
                            ->end()
                        ->end()
                    ->end()
                ->end()
            ->end();

        return $treeBuilder;
    }
}
