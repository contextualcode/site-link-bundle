<?php

namespace ContextualCode\SiteLinkBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

class ContextualCodeSiteLinkExtension extends Extension
{
    public function load(array $configs, ContainerBuilder $container)
    {
        // load config
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        // set parameters
        if (!$container->hasParameter('contextual_code_site_link')) {
            $container->setParameter('contextual_code_site_link', $config);
        }

        // load services
        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
        $loader->load('services.yml');
    }
}
